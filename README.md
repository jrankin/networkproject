Authors: 
	John Rankin
	David Bogle

Assignment: 
	This assignment implements 3 Sliding Window Protocols.
		Stop and Wait
		Go Back N
		Selective Repeat

	
	In order to run, please have 3 seperate terminals open, preferably on different machines.
	
	Run:
		./KDC
		./IDB
		./IDA <Server-Address> <KDC-Address>

	Follow the Instructions.



