LIBS=-lpthread -lrt
CPP= g++
CPPFLAGS= -g -Wall
DEPS= util.h
VFLAGS=--tool=memcheck --leak-check=yes --show-reachable=yes
SHIVA=
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),SunOS)
	LIBS += -lsocket -lnsl -lresolv -DBIG_INDIAN
	SHIVA += -DBIG_INDIAN
endif

all: IDA IDB KDC

IDA: rankinjn_bogled_IDA.o rankinjn_bogled_util.o rankinjn_bogled_blowfish.o rankinjn_bogled_UDPSocket.o rankinjn_bogled_SlidingProtocol.o rankinjn_bogled_StopNWait.o rankinjn_bogled_GBN.o rankinjn_bogled_SR.o
	$(CPP) $(LDFLAGS) $(SHIVA) -o $@ $^ $(LIBS) $(SOLARIS_LIBS) -Wno-write-strings

IDB: rankinjn_bogled_IDB.o rankinjn_bogled_UDPSocket.o rankinjn_bogled_util.o rankinjn_bogled_blowfish.o rankinjn_bogled_SlidingProtocol.o rankinjn_bogled_StopNWait.o rankinjn_bogled_GBN.o rankinjn_bogled_SR.o
	$(CPP) $(LDFLAGS) $(SHIVA) -o $@ $^ $(LIBS) $(SOLARIS_LIBS)	-Wno-write-strings

KDC: rankinjn_bogled_KDC.o rankinjn_bogled_UDPSocket.o rankinjn_bogled_util.o rankinjn_bogled_blowfish.o 
	$(CPP) $(LDFLAGS) $(SHIVA) -o $@ $^ $(LIBS) $(SOLARIS_LIBS)

#%.o: %.c $(DEPS) -DBIG_INDIAN

rankinjn_bogled_KDC.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_KDC.o rankinjn_bogled_KDC.cpp
	
rankinjn_bogled_UDPSocket.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_UDPSocket.o rankinjn_bogled_UDPSocket.cpp

rankinjn_bogled_StopNWait.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_StopNWait.o rankinjn_bogled_StopNWait.cpp -Wno-write-strings
	
rankinjn_bogled_GBN.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_GBN.o rankinjn_bogled_GBN.cpp -Wno-write-strings

rankinjn_bogled_SR.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_SR.o rankinjn_bogled_SR.cpp -Wno-write-strings

rankinjn_bogled_SlidingProtocol.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_SlidingProtocol.o rankinjn_bogled_SlidingProtocol.cpp -Wno-write-strings
	
rankinjn_bogled_IDA.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_IDA.o rankinjn_bogled_IDA.cpp

rankinjn_bogled_IDB.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_IDB.o rankinjn_bogled_IDB.cpp

rankinjn_bogled_util.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_util.o rankinjn_bogled_util.cpp	
	
rankinjn_bogled_blowfish.o:
	g++  -g -Wall -c $(SHIVA) -o rankinjn_bogled_blowfish.o rankinjn_bogled_blowfish.cpp -Wno-missing-braces


valgrind-server:
	valgrind $(VFLAGS) ./server 9458

clean-b:
	rm -f IDB *.o 

clean-a:
	rm -f IDA *.o 

clean:
	rm -f test KDC IDA IDB *.o 
