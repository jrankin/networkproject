#include  <stdio.h>
#include  <signal.h>
#include <ctime>
#include "rankinjn_bogled_GBN.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_util.h"
#include <pthread.h>

#include <list>
#include <semaphore.h>

GBN::GBN(UDPSocket* socket, config_t * config):SlidingProtocol(socket, config) {

    //Init the Semaphore...
    sem_init(&sem_erase, 0, 1);

	sequenceMax = cfg->maxSeq + 1;
	windowSize = cfg->sendingWindowSize;
	sequenceBase = 0;
	
	sFrame.seq = 0;
    rFrame.seq = -1;
}

void GBN::start(){
    if (cfg->isReceiver) {
        startRec();
    }
    else {
        isSend = 1;
        udpSocket.setRecipient(cfg->sendto_url);
        startSend();
    }
}

/*
 * note: this is only being called by the sender now.
 * all the receiver does is update sFrame.seq
 */
void GBN::makeFrame(FILE * fp){

    //initialize data
    //bzero(sFrame.data, data_size);

	// This Variable gets Changed When passed ...
	int dataSize = 0;
	
	// The result tells when it is done.
    SlidingProtocol::readFile(fp, sFrame.data, data_size, dataSize);

	sFrame.dataSize = dataSize;
    //if this is the end of the file, flag it
    if (dataSize < data_size){
        sFrame.flag = END_OF_FILE;
        //isSend = 0;
    }
    else {
        sFrame.flag = DEFAULT;
    }

    sFrame.seq = lastFrameMade;
}

void GBN::timeout(){
	int NUM_SECONDS = 3;
	
	double time_counter = 0;

    clock_t this_time = clock();
    clock_t last_time = this_time;

	while(true) {
		this_time = clock();

		time_counter += (double)(this_time - last_time);

		last_time = this_time;
		fflush(stdout);
		if ( rFrame.seq  == currentFrame  ) {
			cout << "received ack for #" << currentFrame << endl << endl;
			break;
			return ;
		}
		if(time_counter > (double)(NUM_SECONDS * CLOCKS_PER_SEC))
		{
			return;
		}
	}
}

void * GBN::threadTimeout(void *gbn){

	while(true) {
		
		clock_t this_time = clock();
		
        double timeNow;
        timeval time;
        timeNow = currentTime(&time);

		int index = 0;
		int resend = 0;
		sem_wait(&((GBN *) gbn)->sem_erase);
		

		
		for (list<frame>::iterator it = ((GBN *) gbn)->vBuf.begin(); it != ((GBN *) gbn)->vBuf.end(); it++) {
			
			if (index < ((GBN *) gbn)-> windowSize) {
				if(  (*it).timer + ((GBN *)gbn)->cfg->timeout < timeNow && index == 0)
				{
					// Resend All of them..
					resend = 1;
				}
				if (resend) {
					// Reset and Send Frame for ALL 
					(*it).timer = timeNow; // Reset Timer
					((GBN *) gbn)->sendFrame(&*it);
					cout << "Sending Frame: " << (*it).seq << endl;
				}
			}
			index++;
		}
		
		sem_post(&((GBN *) gbn)->sem_erase);	
	}

	return NULL;
}

void GBN::startSend(){
	rFrame.seq = -1;

	pthread_t thread;
	pthread_create(&thread, NULL, GBN::threadGetFrame, this);
	
    FILE *fp;
    fp=fopen(cfg->fileName, "rb");

    //send the first packet to signal start of file upload
    initFrame(&sFrame);
    strcpy(sFrame.data, cfg->fileName);
    sFrame.flag = START_OF_FILE;

    sFrame.seq = 0;

	lastAckNum = -1;
	
	// First Load up the buffer ...
	bzero(sFrame.data, data_size);
    strcpy(sFrame.data, cfg->fileName);
    int ds = string(cfg->fileName).length();
    sFrame.dataSize = ds > data_size ? data_size : ds;
    sFrame.flag = START_OF_FILE;
	
    double timeNow;
    timeval time;
    timeNow = currentTime(&time);
	
	sFrame.timer = timeNow;
	vBuf.push_back(sFrame);
	
	lastFrameMade = (lastFrameMade + 1) % sequenceMax;

	while( (signed int)vBuf.size() <= windowSize && sFrame.flag != END_OF_FILE ) {
		initFrame(&sFrame);
		makeFrame(fp);
		sFrame.seq  = lastFrameMade;
		vBuf.push_back(sFrame);

		if (sFrame.flag == END_OF_FILE) {
			lastAckNum = sFrame.seq;
		}
		lastFrameMade = (lastFrameMade + 1) % sequenceMax;
	}
	
	currentFrame = 0;
	isSend = 1;

	int dontAddAnymore = 0;

	pthread_t thread2;
	pthread_create(&thread2, NULL, GBN::threadTimeout, this);

	while( isSend ) {

		sem_wait(&sem_erase);
		while ( (signed int)vBuf.size() <= windowSize && dontAddAnymore == 0) {
			initFrame(&sFrame);
			makeFrame(fp);
			sFrame.seq = lastFrameMade;
			
			if (sFrame.flag == END_OF_FILE) {
				lastAckNum = sFrame.seq;

				dontAddAnymore = 1; /// Make sure this loop isnt used anymore, because 
									// we have added the last Frame..
			}

			vBuf.push_back(sFrame);
			lastFrameMade = (lastFrameMade + 1) % sequenceMax;
		}
		sem_post(&sem_erase);
		// Send All
	}

    //close the file stream
    fclose(fp);

    //free memory
    destroyFrame(&sFrame);
	
	cout << "File sent.." << endl;
}

void GBN::pop() {
	//destroyFrame(&sFrame);
	frame x  = vBuf.front();
	if (x.flag != END_OF_FILE ) destroyFrame(&x);
	vBuf.pop_front();
}

void * GBN::threadGetFrame(void * gbn)
{
	while(true) {
		((GBN *) gbn)->rFrame.seq = ((GBN *) gbn)->getAck();
		
		if ( ((GBN *) gbn)->rFrame.seq  == ((GBN *) gbn)->currentFrame  ) {

			// Semaphore for thread safe...
			sem_wait(&((GBN *) gbn)->sem_erase);
			
			cout << "received ack for #" << ((GBN *) gbn)->currentFrame << endl << endl;
			
			((GBN *) gbn)->currentFrame = (((GBN *) gbn)->currentFrame + 1) % ((GBN *) gbn)->sequenceMax;
	
			cout << "Current Window =" <<((GBN *) gbn)->makeWindow() << endl;

			// Remove Frame..		
			((GBN *) gbn)->pop();
			
			// Exit sending..
			if (((GBN *) gbn)->rFrame.seq == ((GBN *) gbn)->lastAckNum){
				((GBN *) gbn)->isSend = 0;
			
			}
			sem_post(&((GBN *) gbn)->sem_erase);	
		}
	}
    return NULL;
}

string GBN::makeWindow() {
	// Make Window Display..
	string x = "[";
	
	int temp = currentFrame;
	for (int i=0;i<windowSize;i++) {
		x.append( parseInt(temp ) );
		
		if (windowSize-1 != i) {
				x.append( ", " );
		}
		temp = (temp + 1) % sequenceMax;
	}
	x.append("]");
	return x;
}

void GBN::startRec() {

	int isRec = 1;
	cout << "waiting for packets" << endl;

    FILE * fp2;
    string saveFilePath = "";
	currentFrame = 0;

    //init rFrame so it can be written to
    initFrame(&rFrame);

	while(isRec) {

		getFrame(&rFrame);
		
        //send ack for received frame
        sendAck(rFrame.seq);
        cout << "Sent ack for " << rFrame.seq << endl;
		
        //only accept frame if it's the one we want
		if (currentFrame == rFrame.seq && isRec) {

			currentFrame = (currentFrame + 1) % sequenceMax;
			cout << "Current Window = [" << currentFrame << "]" << endl;
			
            switch ( rFrame.flag ) {
                case START_OF_FILE:
					saveFilePath.append(rFrame.data, 0, rFrame.dataSize);
					saveFilePath.append("2");
					saveFilePath = "/tmp/outDJ";
                    fp2=fopen((char *) "/tmp/outDJ", "wb");
					
                    cout << "Writing to " << saveFilePath << endl;
					currentFrame = 1;
                    initFrame(&rFrame); //init rFrame so it can be written to
                    break;

                case END_OF_FILE:
					cout << "ending file..." << endl;
					SlidingProtocol::writeFile(fp2, rFrame.data, rFrame.dataSize);
					currentFrame = -1; //End File
					fclose(fp2);
					cout << endl << "File transfer for " << saveFilePath << " is complete." << endl;
                  //  destroyFrame(&rFrame); //free memory
					break;

                case DEFAULT:
                    SlidingProtocol::writeFile(fp2, rFrame.data, rFrame.dataSize);
                    break;

                default:
                    cout << "ERROR: bad flag value of " << rFrame.flag << endl;
                    exit(1);
                    break;
            }
		}
	}
}
