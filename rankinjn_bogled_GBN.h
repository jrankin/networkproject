#ifndef GBN_H
#define GBN_H

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_blowfish.h"
#include "rankinjn_bogled_SlidingProtocol.h"

using namespace std;


class GBN : public SlidingProtocol 
{

public:
    GBN(UDPSocket* udpSocket, config_t * cfg);
	void startRec();
	void startSend();
	void pop();
	void makeFrame(FILE * fp);
    void start();
	static void* threadGetFrame(void *frame);
	static void* threadTimeout(void *frame);
	string makeWindow();
	void timeout();

	// Variables
	sem_t sem_erase;
	int lastAckNum;
	int lastFrameMade;
	int resendData;
	int windowSize;
	int sequenceBase;
	int sequenceMax;
	int currentFrame; // Sequence Number
	double time_counter;
	int startTimer;
	list<frame> vBuf;
	int keepSending;
	int isSend;
	frame sFrame; //sending frame
	frame rFrame; //receiving frame
    size_t fileSize;
};
#endif
