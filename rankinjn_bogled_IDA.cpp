#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#include "rankinjn_bogled_IDA.h"
#include "rankinjn_bogled_SlidingProtocol.h"
#include "rankinjn_bogled_StopNWait.h"
#include "rankinjn_bogled_GBN.h"
#include "rankinjn_bogled_SR.h"

int main(int argc, char** argv) {

    if(argc != 3){
        printf("Usage : %s <Server-Address> <KDC-Address>\n", argv[0]);
        exit(0);
    }

    IDA ida(argv);
    ida.start();
}


IDA::IDA(char** params) {

    idb_host = params[1];
    kdc_host = params[2];

	udpSocket.getSocket();
	udpSocket.getPort();
	
    //getPortspecify port of receiver 
	bzero(&udpSocket.cli_addr, sizeof(udpSocket.cli_addr));
	udpSocket.cli_addr.sin_family = AF_INET;
	udpSocket.cli_addr.sin_port = htons(PORT);

    //initialize fields
    str = new char[BUFLEN]();
	bzero(str, BUFLEN);
    buffer = new char[ID_LEN + NONCE_LEN]();
	bzero(buffer, ID_LEN + NONCE_LEN);
    sessionKey = new char[KEY_LEN]();
	bzero(sessionKey, KEY_LEN);
    bEnc = new char[B_ENC_LEN]();
	bzero(bEnc, B_ENC_LEN);
}

// Algorithm
void IDA::start() {	

    cout << "  _____ _____                   "<< endl;
    cout << " |_   _|  __ \\                 "<< endl;
    cout << "   | | | |  | |  ______    __ _ " << endl;
    cout << "   | | | |  | | |______|  / _` |" << endl;
    cout << "  _| |_| |__| |          | (_| |" << endl;
    cout << " |_____|_____/            \\__,_|" << endl;
    cout << "" << endl;
    cout << "" << endl;
    cout << "" << endl;
    cout << "=====================================" << endl;
    cout << "=         KDC Algorithm Setup       =" << endl;
    cout << "=====================================" << endl;
    cout << endl;

    nonce = getNonceFromUser("Enter the nonce for A: ");
    keyA  = getKeyFromUser("Enter the key for A: ");

	// Sliding Protocol Setup....
    SlidingProtocol * st;
    config_t * cfg = (config_t*)malloc(sizeof(config_t));
    cfg->BF = &BF;
  //  cfg->fileName = (char *)"testfile.txt";
    //cfg->sendto_url = (char *) malloc(strlen(kdc_host) + 1 );
	//cfg->sendto_url =  idb_host;
    //cfg->sessionKey = (char *)"secretkey";

    cfg->isReceiver = false;

    //get the rest of the configuration from the user
    configFromUser(cfg);

    if( cfg->algorithm == config_t::SW ){
	    st = new StopNWait(&udpSocket, cfg);
    }
    else if( cfg->algorithm == config_t::GBN ){
	    st = new GBN(&udpSocket, cfg);
    }
    else{
        st = new SR(&udpSocket, cfg);
    }

	// This Runs the KDC Algorithm
	step1();
	step2();
	step3();
	step4();
	step5();
//	strcpy ( cfg->sessionKey, sessionKey[ );
    //	cfg->sessionKey = &sessionKey[0];
//	cout << "THE SESSION KEY IS" << endl;
	udpSocket.getData((char *)str, 8);
	
	decrypt(&BF, (int*)str, 8);
	string start = string(str);

	string checkStart = "start";

	if (start.compare(checkStart) == 0) {
		cout << "Starting Sliding Protocol..." << endl;
	} else {
		exit(1);
	}

	cfg->sendto_url =  idb_host;
    cfg->sessionKey = &sessionKey[0];

    //variables to time the execution time
    timeval time;
    double t1, t2;

    //run the transmission
    st->calibrateTimeout();

    t1 = currentTime(&time);
    st->start();
    t2 = currentTime(&time);

    cout << "Total Time: " << t2 - t1 << " seconds" << endl;

    if( cfg->errors == config_t::MANUAL ){
        free(cfg->packetsLost);
        free(cfg->checksumErrors);
    }
    free(cfg->fileName);
    free(cfg);

}


void IDA::step1() {
    //step 1: send request and nonce to KDC

    //first set the recipient to be IDB, so we can get that IP address
    udpSocket.setRecipient(this->idb_host);
    char buffer[ID_LEN + NONCE_LEN];
    bzero(buffer, ID_LEN + NONCE_LEN);
    // string ipB = string(inet_ntoa(cli_addr.sin_addr));

    string ipB = udpSocket.getIP();
    //copy the ID of B and the nonce into the buffer
    strncpy(buffer, ipB.c_str(), ID_LEN);
    strncpy(buffer + ID_LEN, nonce.c_str(), NONCE_LEN);

    cout << endl;
    cout << "STEP 1:" << endl;
    cout << "  sent id of B: " << ipB << endl;
    cout << "  sent nonce:   " << nonce << endl;
    cout << endl;

    //set the recipient to the KDC
    udpSocket.setRecipient(this->kdc_host);

    //ship it off
    udpSocket.sendData(buffer, ID_LEN + NONCE_LEN);
}

void IDA::step2() {
    //step 2: parse the response from the KDC

    strcpy(str, keyA.c_str());
    BF.Set_Passwd(str);

    udpSocket.getData((char *)str, A_ENC_LEN);

    cout << "STEP 2:" << endl;
    cout << "  received: " << string_to_hex(str, A_ENC_LEN) << endl;

    decrypt(&BF, (int*)str, A_ENC_LEN);

    strncpy(sessionKey, str, KEY_LEN);

    cout << "  session key:   " << sessionKey << endl;
    cout << "  message for B: " << string_to_hex(str + KEY_LEN + ID_LEN + NONCE_LEN, B_ENC_LEN) << endl;
    cout << endl;
}

void IDA::step3() {
    //step 3: send stuff to B

    char bEnc[B_ENC_LEN];
    bzero(bEnc, B_ENC_LEN);

    //copy the data for B, from the KDC, 
    //into the buffer we're going to send
    strncpy(bEnc, str + KEY_LEN + ID_LEN + NONCE_LEN, B_ENC_LEN);

    udpSocket.setRecipient(this->idb_host);

    cout << "STEP 3:" << endl;
    cout << "  sent: " << string_to_hex(bEnc, B_ENC_LEN) << endl;
    cout << endl;

    udpSocket.sendData(bEnc, B_ENC_LEN);
}

void IDA::step4() {
    //step 4: get nonce2 from B

    udpSocket.getData((char *)str, NONCE_ENC_LEN);

    BF.Set_Passwd(sessionKey);

    decrypt(&BF, (int*)str, NONCE_ENC_LEN);
    nonce2 = string(str);

    cout << "STEP 4:" << endl;
    cout << "  received nonce: " << nonce2 << endl;
    cout << endl;
}

void IDA::step5() {
    //step 5: calculate f(n) and send to B

    string hashedNonce = nonceHash(nonce2);

    if( hashedNonce.compare("NaN") != 0 ){

        char sendBuffer[NONCE_ENC_LEN];
        bzero(sendBuffer, NONCE_ENC_LEN);

        strncpy(sendBuffer, hashedNonce.c_str(), NONCE_ENC_LEN);

        cout << "STEP 5: " << endl;
        cout << "  sent f(n): " << sendBuffer << endl;	
        cout << endl;

        encrypt(&BF, (int*)sendBuffer, NONCE_ENC_LEN);

        udpSocket.sendData(sendBuffer, NONCE_ENC_LEN);
    }
    else{
        cout << "Error: Nonce received was not a number." << endl;
        exit(1);
    }
}
