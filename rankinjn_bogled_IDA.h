#ifndef IDA_H
#define IDA_H

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_IDA.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_blowfish.h"

using namespace std;

class IDA  {
private:

	// parameters
	char** argv;
	UDPSocket udpSocket;
  string nonce;
	string keyA;
	string ipB;
	string nonce2;
	char * idb_host;
	char * kdc_host;

	// Blowfish
	Blowfish BF;
	// Variables For Making Messages
	char * str;
	char * buffer;
	char * sessionKey;
	char * bEnc;
	
	void step1();
	void step2();
	void step3();
	void step4();
	void step5();

public:
  IDA(char**);
	void start();

};
#endif
