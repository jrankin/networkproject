#include "rankinjn_bogled_IDB.h"
#include <stdio.h>
#include <stdlib.h>
#include "rankinjn_bogled_SlidingProtocol.h"
#include "rankinjn_bogled_StopNWait.h"
#include "rankinjn_bogled_GBN.h"
#include "rankinjn_bogled_SR.h"
#include "rankinjn_bogled_util.h"

// Constructor
IDB::IDB() {
	
    cout << "=====================================" << endl;
	cout << "=         KDC Algorithm Setup       =" << endl;
	cout << "=====================================" << endl;
	cout << endl;
	
    //get input from user
	nonce = getNonceFromUser("Enter the nonce for B: ");
	keyB  = getKeyFromUser("Enter the key for B: ");
	
    //initialize fields
    step3_message = new char[B_ENC_LEN]();
    bzero(step3_message, B_ENC_LEN);
    step4_message = new char[NONCE_ENC_LEN]();
    bzero(step4_message, NONCE_ENC_LEN);
    step5_message = new char[NONCE_ENC_LEN]();
    bzero(step5_message, NONCE_ENC_LEN);
    sessionKey = new char[KEY_LEN + 1]();
    bzero(sessionKey, KEY_LEN + 1);
    idA = new char[ID_LEN]();
    bzero(idA, ID_LEN);
    str = new char[BUFLEN]();
    bzero(str, BUFLEN);

    //bind to our socket
    udpSocket.getSocket();
    udpSocket.bindToSocket();
}

// Algorithm
void IDB::start() {	
	// Sliding Protocol Setup...
	SlidingProtocol * st;
    config_t * cfg = (config_t*)malloc(sizeof(config_t));
    cfg->BF = &BF;
    cfg->isReceiver = true;

    //get the rest of the configuration from the user
    configFromUser(cfg);

    if( cfg->algorithm == config_t::SW ){
	    st = new StopNWait(&udpSocket, cfg);
    }
    else if( cfg->algorithm == config_t::GBN ){
	    st = new GBN(&udpSocket, cfg);
    }
    else{
        st = new SR(&udpSocket, cfg);
    }

	// KDC Algorithm
	
	step3();
	step4();

	// If The Program hasn't Exited... We can start the Sliding Protocol,
	// But First we need to send 1 more message to Let The IDA know to start
	// Sending the File..
	char startProtocol[8];
	char* startProtocol2 = "start";
	
	bzero(startProtocol,8);
	strncpy(startProtocol,startProtocol2,8);
	
	encrypt(&BF, (int *)startProtocol, 8);
	udpSocket.sendData(startProtocol, 8);

	

	// Set the Session Key...
	cfg->sessionKey = &sessionKey[0];
	
	
	// Start Sliding Protocol
    st->calibrateTimeout();
    st->start();
    
    if( cfg->errors == config_t::MANUAL ){
        free(cfg->packetsLost);
        free(cfg->checksumErrors);
    }
    free(cfg);
	
}

void IDB::step3() {

  udpSocket.getData(step3_message, B_ENC_LEN);
  cout << endl;
  cout << "STEP 3:" << endl;
  cout << "  message:     " << string_to_hex(step3_message, B_ENC_LEN) << endl;

  strcpy(str, keyB.c_str());
  BF.Set_Passwd(str);
  decrypt(&BF, (int*)step3_message, B_ENC_LEN);

  //get session key
  strncpy(sessionKey, step3_message, KEY_LEN);

  //get ID of A
  strncpy(idA, step3_message + KEY_LEN, ID_LEN);
  
  cout << "  session key: " << sessionKey << endl;
  cout << "  id of A:     " << idA << endl;
  cout << endl;
  
  strncpy(step4_message, nonce.c_str(), NONCE_LEN);
}

void IDB::step4() {

  string step4Final(step4_message);

  string randomNum = nonceHash(step4Final);
  step4Final = string(randomNum);
  
  BF.Set_Passwd(sessionKey);
  encrypt(&BF, (int*)step4_message, NONCE_ENC_LEN);

  cout << "STEP 4:" << endl;
  cout << "  sent nonce: " << nonce << endl;
  cout << endl;

  udpSocket.sendData(step4_message, NONCE_ENC_LEN);

	if (!isdigit(nonce.c_str()[0])) {
		cout << endl << endl << "Wrong Key From A ...exiting " << endl << endl;
		exit(1);
	}
  udpSocket.getData(step5_message, NONCE_ENC_LEN);
  
  decrypt(&BF, (int*)step5_message, NONCE_ENC_LEN);

	  cout << "STEP 5:" << endl;
	  cout << "  expected f(n): " << step4Final << endl;
	  cout << "  received f(n): " << step5_message << endl;
	  cout << endl;
	  
	  int result1  = boost::lexical_cast<int>(step4Final);
	  int result2  = boost::lexical_cast<int>(step5_message);
	  if (result1 == result2) {
			cout << "They match..." << endl;
	  }
	  
}

int main() {
	

	cout << "  _____ _____             _     " << endl;
	cout << " |_   _|  __ \\           | |    " << endl;
	cout << "   | | | |  | |  ______  | |__  " << endl;
	cout << "   | | | |  | | |______| | '_ \\ " << endl;
	cout << "  _| |_| |__| |          | |_) |" << endl;
	cout << " |_____|_____/           |_.__/ " << endl;
	cout << "" << endl;
	cout << "" << endl;
	cout << "" << endl;
	
 
	IDB idb;
	idb.start();
}
