#ifndef IDB_H
#define IDB_H

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_IDB.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_blowfish.h"

using namespace std;

class IDB {
private:
	UDPSocket udpSocket;
	
  string nonce;
	string keyB;
	
	// Blowfish
	Blowfish BF;
	
	// Variables For Making Messages
	char * step3_message;
	char * step4_message;
	char * step5_message;
	char * sessionKey;
	char * idA;
	char * str;
	string step4Final;

	void step3();
	void step4();

public:
  IDB();
	void start();

};
#endif
