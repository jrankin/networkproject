#include "rankinjn_bogled_KDC.h"
#include <stdio.h>
#include <stdlib.h>

// Constructor
KdcKey::KdcKey() {

	//initialize fields
    id_b = new char[ID_LEN + 1]();
    bzero(id_b, ID_LEN + 1);
    nonce = new char[NONCE_LEN + 1]();
    bzero(nonce, NONCE_LEN + 1);
    a_enc = new char[A_ENC_LEN]();
    bzero(a_enc, A_ENC_LEN);
    b_enc = new char[B_ENC_LEN]();
    bzero(b_enc, B_ENC_LEN);
    str = new char[BUFLEN]();
	bzero(str, BUFLEN);
}

//do handshake protocol
void KdcKey::start() {	

    cout << " _  _______   _____ " << endl;
    cout << "| |/ /  __ \\ / ____|" << endl;
    cout << "| ' /| |  | | |     " << endl;
    cout << "|  < | |  | | |     " << endl;
    cout << "| . \\| |__| | |____ " << endl;
    cout << "|_|\\_\\_____/ \\_____|" << endl;
    cout << endl;

    //get input from user
    sessionKey = getKeyFromUser("Enter the session key: ");
    keyA       = getKeyFromUser("Enter the key for A: ");
    keyB       = getKeyFromUser("Enter the key for B: ");

    //bind to our socket
    UDPSocket::getSocket();
    UDPSocket::bindToSocket();

    while( 1 ){
        cout << "=====================================" << endl << endl;
        step1();
        step2();
        step3();
        step4();
    }
}

void KdcKey::step1() {
  //get request

  UDPSocket::getData(str, ID_LEN + NONCE_LEN);

  strncpy(id_b, str, ID_LEN);
  strncpy(nonce, str + ID_LEN, NONCE_LEN);

  string idA = string(inet_ntoa(cli_addr.sin_addr));

  cout << endl;
  cout << "STEP 1:" << endl;
  cout << "  request from:      " << idA << endl;
  cout << "  requested ID of B: " << id_b << endl;
  cout << "  nonce:             " << nonce << endl;
  cout << endl;

  //append id of A to session key
  strncpy(b_enc, sessionKey.c_str(), KEY_LEN);
  strncpy(b_enc + KEY_LEN, idA.c_str(), ID_LEN);
}

void KdcKey::step2() {
  //step 2: encrypt stuff from step 1 with B's key
  strcpy(str, keyB.c_str());
  BF.Set_Passwd(str);

  encrypt(&BF, (int*)b_enc, B_ENC_LEN);
  cout << "STEP 2: " << endl;
  cout << "  message for B:    " << string_to_hex(b_enc, B_ENC_LEN) << endl;
  cout << "  session key:      " << sessionKey << endl;
}

void KdcKey::step3() {
  //step 3: take the session key and append the original request,
  //and then append what we got in step 2

  strncpy(a_enc, sessionKey.c_str(), KEY_LEN);
  strncpy(a_enc + KEY_LEN, id_b, ID_LEN);
  strncpy(a_enc + KEY_LEN + ID_LEN, nonce, NONCE_LEN);
  strncpy(a_enc + KEY_LEN + ID_LEN + NONCE_LEN, b_enc, B_ENC_LEN);
}

void KdcKey::step4() {
  //step 4: encrypt all that with A's key and send it back
  strcpy(str, keyA.c_str());
  BF.Set_Passwd(str);
  encrypt(&BF, (int*)a_enc, A_ENC_LEN);
  cout << "  encryption for A: " << string_to_hex(a_enc, A_ENC_LEN) << endl;
  cout << endl;

  UDPSocket::sendData(a_enc, BUFLEN);
}

int main() {
	
	KdcKey kdc;
	kdc.start();
}
