#ifndef KDCKEY_H
#define KDCKEY_H

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_blowfish.h"

using namespace std;

class KdcKey: public UDPSocket {
private:

  string sessionKey;
	string keyA;
	string keyB;
	
	// Blowfish
	Blowfish BF;
	
	// Variables For Making Messages
	char * id_b;
	char * nonce;
	char * a_enc;
	char * b_enc;
	char * str;
	
	string getMessageA();
	string getMessageB();
	void step1();
	void step2();
	void step3();
	void step4();

public:
  KdcKey();
	void start();
	
};
#endif
