#include <stdio.h>
#include <signal.h>
#include <ctime>
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_SR.h"

#include <pthread.h>
#include <list>
#include <semaphore.h>

SR::SR(UDPSocket* socket, config_t * config):SlidingProtocol(socket, config){

	// Init the Semaphore
	sem_init(&semaphore, 0, 1);

    if( cfg->isReceiver ){
        windowSize = cfg->receivingWindowSize;
    }
    else{
        windowSize = cfg->sendingWindowSize;
    }

    maxSeq = cfg->maxSeq + 1;
    windowBase = 0;
    lastAckNum = INT_MAX;
    saveFilePath = "";
    acksNeeded = 0;

    //init frames
    frameArray = (SlidingProtocol::frame *)malloc(maxSeq * sizeof(SlidingProtocol::frame));
    for( int i=0; i<maxSeq; i++ ){
        initFrame(&frameArray[i]);
    }
}

void SR::start(){

    if (cfg->isReceiver) {
        startRec();
    }
    else {
        keepSending = true;
        udpSocket.setRecipient(cfg->sendto_url);
        startSend();
    }

    //free memory
    for( int i=0; i<maxSeq; i++ ){
        destroyFrame(&frameArray[i]);
    }
    free(frameArray);
}

bool SR::makeFrame(SlidingProtocol::frame * frame, FILE * fp){

    //initialize data
    bzero(frame->data, data_size);

	// This Variable gets Changed When passed ...
	int dataSize = 0;
	
	// The result tells when it is done.
    SlidingProtocol::readFile(fp, frame->data, data_size, dataSize);
	frame->dataSize = dataSize;

    //if this is the end of the file, flag it
    if (dataSize < data_size){
        frame->flag = END_OF_FILE;
    }
    else {
        frame->flag = DEFAULT;
    }

    frame->timer = 0;
    frame->isAcked = false;

    //return true if there's more to be read
    return frame->flag == DEFAULT;
}

bool SR::bufferFrames(int startIndex, int howMany, FILE * fp){
    
    bool moreToRead = true;

    for( int i=startIndex; moreToRead && i<startIndex + howMany; i++ ){
        moreToRead = makeFrame(&(frameArray[i%maxSeq]), fp);
        frameArray[i%maxSeq].seq = i%maxSeq;
        frameArray[i%maxSeq].timer = 0;

        if( !moreToRead ){
            lastAckNum = i % maxSeq;
        }
    }

    //will be false if the file is completely read
    return moreToRead;
}

void * SR::threadGetAck(void * sr){

    while( true ){

        int frameAcked = ((SR *)sr)->getAck();

        sem_wait(&((SR*)sr)->semaphore);

        cout  << ">> ACK " << frameAcked << endl;

        if( frameAcked == ((SR*)sr)->lastAckNum ){
            ((SR*)sr)->keepSending = false;
        }

        if( ((SR*)sr)->frameInWindow(frameAcked) ){
            ((SR*)sr)->frameArray[frameAcked].isAcked = true;
            ((SR*)sr)->acksNeeded--;

            if( ((SR*)sr)->keepSending && frameAcked == ((SR*)sr)->windowBase ){
                int i = ((SR*)sr)->windowBase;
                while( ((SR*)sr)->frameArray[i].isAcked && ((SR*)sr)->windowMoved < ((SR*)sr)->windowSize){
                    ((SR*)sr)->windowMoved++;
                    i = (i + 1) % ((SR*)sr)->maxSeq;
                }
                ((SR*)sr)->windowBase = i;
            }
        }

        sem_post(&((SR*)sr)->semaphore);
    }

    return NULL;
}

void SR::startSend(){

	pthread_t thread;
	pthread_create(&thread, NULL, SR::threadGetAck, this);
	
    FILE *fp;
    fp=fopen(cfg->fileName, "rb");

    //load up the initial frames
    bzero(frameArray[0].data, data_size);
    strcpy(frameArray[0].data, cfg->fileName);
    int ds = string(cfg->fileName).length();
    frameArray[0].dataSize = ds > data_size ? data_size : ds;
    frameArray[0].flag = START_OF_FILE;
    frameArray[0].seq = 0;

    //note that the first frame has already been set/buffered
    bool moreToRead = bufferFrames(1, windowSize-1, fp);

    while( keepSending ){
        
        sem_wait(&semaphore);

        if( windowMoved > 0 ){

            cout << "window: [";
            for( int i=0; i<windowSize; i++ ){
                cout << (windowBase + i)%maxSeq << " ";
            }
            cout << "]" << endl;

            if( moreToRead ){
                moreToRead = bufferFrames((windowBase + windowSize - windowMoved )%maxSeq, windowMoved, fp);
            }

            windowMoved = 0;
        }

        SlidingProtocol::frame * f;

        double timeNow;
        timeval time;
        timeNow = currentTime(&time);
        
        for( int i=windowBase; i<(windowBase + windowSize) && keepSending; i++ ){
            
            f = &frameArray[i%maxSeq];            
            
            if( i%maxSeq <= lastAckNum && 
                    !f->isAcked 
                    && f->timer > -1 
                    && (f->timer + cfg->timeout) < timeNow ){

                if( f->timer ) cout << "(timeout) ";
                cout << "<< SENT " << f->seq << ":   FLAG:" << f->flag << endl;
                sendFrame(f);
                f->timer = timeNow;
            }
        }

        sem_post(&semaphore);
    }


    /*
     * make sure all frames have been acked
     */

    //count how many acks we still need
    acksNeeded = 0;
    for( int i=windowBase; i<(windowBase + windowSize); i++ ){
        if( i%maxSeq <= lastAckNum && !frameArray[i%maxSeq].isAcked ){
            acksNeeded++;
        }
    }

    SlidingProtocol::frame * f;
    while( acksNeeded ){
        double timeNow;
        timeval time;
        timeNow = currentTime(&time);

        for( int i=windowBase; i<(windowBase + windowSize); i++ ){

            f = &frameArray[i%maxSeq];            

            if( i%maxSeq <= lastAckNum && !f->isAcked && (f->timer + cfg->timeout) < timeNow ){

                if( f->timer ) cout << "(timeout) ";
                cout << "sending frame " << f->seq << endl;
                sendFrame(f);
                f->timer = timeNow;
            }
        }
    }

    //close the file stream
    fclose(fp);
	
	cout << "[Transmission complete]" << endl;
}

void SR::startRec() {

	cout << "waiting for packets" << endl;

    savefp = NULL;

    initFrame(&rFrame);

	while(true) {

		getFrame(&rFrame);
		
        //send ack for received frame
        cout << "Seq:" << rFrame.seq << "\tFLAG:" << rFrame.flag << endl;
        sendAck(rFrame.seq);
        cout << endl << ">> ACK " << rFrame.seq << endl;

        if( windowMoved > 0 ){
            cout << "window: [";
            for( int i=0; i<windowSize; i++ ){
                cout << (windowBase + i)%maxSeq << " ";
            }
            cout << "]" << endl;
        }
		
        if( frameInWindow(rFrame.seq) ){

            memcpy(frameArray[rFrame.seq].data, rFrame.data, rFrame.dataSize);
            frameArray[rFrame.seq].seq = rFrame.seq;
            frameArray[rFrame.seq].flag = rFrame.flag;
            frameArray[rFrame.seq].dataSize = rFrame.dataSize;
            frameArray[rFrame.seq].isAcked = true;

            if( rFrame.seq == windowBase ){
                int i = windowBase;
                while( frameArray[i].isAcked ){

                    processFrame(&(frameArray[i]));
                    frameArray[i].isAcked = false;
                    windowMoved++;
                    i = (i + 1) % maxSeq;
                }
                windowBase = i;
            }
        }
	}

    destroyFrame(&rFrame);
}


void SR::processFrame(SlidingProtocol::frame * frame){

    switch ( frame->flag ) {
        case START_OF_FILE:
            saveFilePath.append(frame->data, 0, frame->dataSize);
            saveFilePath.append("2");
			saveFilePath = "/tmp/outDJ";
            savefp=fopen((char *) "/tmp/outDJ", "wb");
            cout << "Writing to " << saveFilePath << endl;
            break;

        case END_OF_FILE:
            SlidingProtocol::writeFile(savefp, frame->data, frame->dataSize);
            fclose(savefp);
            cout << endl << "File transfer for " << saveFilePath << " is complete." << endl;
			saveFilePath.clear();
            break;

        case DEFAULT:
            SlidingProtocol::writeFile(savefp, frame->data, frame->dataSize);
            break;

        default:
            cout << "ERROR: bad flag value of " << frame->flag << endl;
            exit(1);
            break;
    }
}

bool SR::frameInWindow(int seqNum){

    bool frameInWindow;

    if( (windowBase + windowSize) > maxSeq ){
        frameInWindow = seqNum >= windowBase || seqNum < ((windowBase + windowSize) % maxSeq);
    }
    else{
        frameInWindow = seqNum >= windowBase && seqNum < windowBase + windowSize;
    }

    return frameInWindow;
}

