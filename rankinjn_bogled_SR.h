#ifndef SR_H
#define SR_H

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_blowfish.h"
#include "rankinjn_bogled_SlidingProtocol.h"

using namespace std;

class SR : public SlidingProtocol 
{

public:
    SR(UDPSocket* udpSocket, config_t * cfg);
    void start();
	void startRec();
	void startSend();
    bool makeFrame(SlidingProtocol::frame * frame, FILE * fp);
    bool bufferFrames(int startIndex, int howMany, FILE * fp);
    void processFrame(SlidingProtocol::frame * frame);
    bool frameInWindow(int seqNum);
	static void * threadGetAck(void * sr);

	// Variables
	int windowSize;
    int windowBase;
    int maxSeq;
	bool keepSending;
    int windowMoved;
    int lastAckNum;
    string saveFilePath;
    frame * frameArray;
	frame rFrame; //receiving frame
    FILE * savefp;
    int acksNeeded;
	sem_t semaphore;
};

#endif
