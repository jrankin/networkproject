#include "rankinjn_bogled_SlidingProtocol.h"
#include "rankinjn_bogled_UDPSocket.h"
#include <iostream>
#include <fstream>
#include <cstring> // for std::strlen
#include <cstddef> // for std::size_t -> is a typedef on an unsinged int


SlidingProtocol::SlidingProtocol(UDPSocket * sock, config_t * config) {

    udpSocket = *sock;
    cfg = config;
	
  //  cfg->BF->Set_Passwd(cfg->sessionKey);
	
    data_size = cfg->data_size;
    buffer_size = nextMult8(FRAME_SIZE);
    checksummed_buf_size = CHECKSUMMED_BUF_SIZE;

    if( cfg->errors == config_t::AUTO ){
        //seed the random number generator used in error creation
        srand((unsigned)time(NULL));
    }
}

void SlidingProtocol::initFrame(frame * f){
    f->data = (char*)malloc(data_size);
    bzero(f->data, data_size);
}

void SlidingProtocol::destroyFrame(frame * f){
    free(f->data);
    f->data = NULL;
}

void SlidingProtocol::writeFile(FILE *fp, char *str, int howMany) {

    fwrite(str, sizeof(char), howMany, fp);
} 

int SlidingProtocol::readFile(FILE *fp, char *str, int howMany, int &dataSize ) {
	
	int isDone = 0;

    //don't need to use fseek anymore
    //this automatically advances fp
    int result = fread(str, sizeof(char), howMany, fp);

	if (result < howMany) {
		isDone = 1;
	}
	dataSize = result;
	
	return isDone;
} 

size_t SlidingProtocol::fileSize(char *fileName){

	ifstream inFile;
	size_t size = 0;

	inFile.open( fileName, ios::in|ios::binary|ios::ate );
	
	inFile.seekg(0, ios::end); // set the pointer to the end
	size = inFile.tellg() ; // get the length of the file

	return size;
}

void SlidingProtocol::encrypt(char * buf, int bufsize){

    //portray data as ints so we can fix endianness
    int * intbuf = (int*)buf;

    //encrypt data
    for( unsigned int i=0; i<bufsize/sizeof(int); i++ ){
        intbuf[i] = htonl(intbuf[i]);
    }

    cfg->BF->Encrypt(intbuf, bufsize);

    for( unsigned int i=0; i<bufsize/sizeof(int); i++ ){
        intbuf[i] = ntohl(intbuf[i]);
    }
}

void SlidingProtocol::decrypt(char * buf, int bufsize){

    int * intbuf = (int*)buf;

    //decrypt data
    for( unsigned int i=0; i<bufsize/sizeof(int); i++ ){
      intbuf[i] = htonl(intbuf[i]);
    }

    cfg->BF->Decrypt(intbuf, bufsize);

    for( unsigned int i=0; i<bufsize/sizeof(int); i++ ){
      intbuf[i] = ntohl(intbuf[i]);
    }
}

void SlidingProtocol::structToBuf(SlidingProtocol::frame * frame, char * buf){
    /*
     * note that the order that memory is copied into the buffer is extremely important
     * and must reflect the order in bufToStruct.
     * It is also important that the checksum is the last thing in the buffer, and that
     * the rest of the buffer is a 16-bit multiple so the checksum can work.
     */
	 
	frame->seq = htons(frame->seq);
	frame->dataSize = htons(frame->dataSize);

    memcpy(buf, frame->data, data_size);
    memcpy(buf + data_size, &(frame->seq), sizeof(frame->seq));
    memcpy(buf + data_size + sizeof(frame->seq), &(frame->dataSize), sizeof(frame->dataSize));
    memcpy(buf + data_size + sizeof(frame->seq) + sizeof(frame->dataSize), &(frame->flag), sizeof(frame->flag));

    frame->seq = ntohs(frame->seq);
    frame->dataSize = ntohs(frame->dataSize);
}

void SlidingProtocol::bufToStruct(SlidingProtocol::frame * frame, char * buf){

    memcpy(frame->data, buf, data_size);
    memcpy(&(frame->seq), buf + data_size, sizeof(frame->seq));
    memcpy(&(frame->dataSize), buf + data_size + sizeof(frame->seq), sizeof(frame->dataSize));
    memcpy(&(frame->flag), buf + data_size + sizeof(frame->seq) + sizeof(frame->dataSize), sizeof(frame->flag));
    memcpy(&(frame->checksum), buf + this->checksummed_buf_size, sizeof(frame->checksum));

    frame->seq = ntohs(frame->seq);
    frame->dataSize = ntohs(frame->dataSize);
    frame->checksum = ntohs(frame->checksum);
}

void SlidingProtocol::sendAck(int sequenceNumber){

    //only send the ack if we don't have a packet lost error
    if( sequenceNumber != ACK_PING && ((cfg->errors == config_t::AUTO && ((double)rand()/(double)RAND_MAX) < cfg->packetLostRatio ) || (cfg->errors == config_t::MANUAL && cfg->packetsLost->size() && cfg->packetsLost->front() == sequenceNumber))){

        cout << "XXX ack lost: " << sequenceNumber << endl; 
        if( cfg->errors == config_t::MANUAL ){
            cfg->packetsLost->pop_front();
        }
    }
    else{
        char buffer[ACK_SIZE];
        sequenceNumber = htonl(sequenceNumber);
        memcpy(buffer, &sequenceNumber, sizeof(sequenceNumber));
        udpSocket.sendData(buffer, ACK_SIZE);
    }
}

int SlidingProtocol::getAck(){
    char buffer[ACK_SIZE];
    int sequenceNumber = -1;

    udpSocket.getData(buffer, ACK_SIZE);
    memcpy(&sequenceNumber, buffer, sizeof(sequenceNumber));

    sequenceNumber = ntohl(sequenceNumber);

    return sequenceNumber;
}

void SlidingProtocol::sendFrame(SlidingProtocol::frame * frame){

    char buffer[buffer_size];
    bzero(buffer, buffer_size);

    structToBuf(frame, buffer);

    addChecksum(buffer);

    //if we have a checksum error, tweak a random bit
    if( frame->flag != PING
        && ((cfg->errors == config_t::AUTO && ((double)rand()/(double)RAND_MAX) < cfg->checksumRatio )
        || (cfg->errors == config_t::MANUAL && cfg->checksumErrors->size() && cfg->checksumErrors->front() == frame->seq ))){
        //randomly select one of the bytes in the payload
        int byte_to_kill = rand() % CHECKSUMMED_BUF_SIZE;

        //flip the first bit in that byte
        cout << "XXX checksum err: " << frame->seq << endl;
        buffer[byte_to_kill] = buffer[byte_to_kill] ^ 1;

        if( cfg->errors == config_t::MANUAL ){
            cfg->checksumErrors->pop_front();
        }
    }
	
    if( cfg->encryption ){
        encrypt(buffer, buffer_size);
    }
	
    //only send the frame if we don't have a packet lost error
    if( frame->flag != PING 
        && ((cfg->errors == config_t::AUTO && ((double)rand()/(double)RAND_MAX) < cfg->packetLostRatio )
        || (cfg->errors == config_t::MANUAL && cfg->packetsLost->size() && cfg->packetsLost->front() == frame->seq ))){
        cout << "XXX packet lost: " << frame->seq << endl; 
        if( cfg->errors == config_t::MANUAL ){
            cfg->packetsLost->pop_front();
        }
    }
    else{
        udpSocket.sendData(buffer, buffer_size);
    }
}

void SlidingProtocol::calibrateTimeout(){

    frame * f = (frame*)malloc(sizeof(frame));
    initFrame(f);

    if( cfg->isReceiver ){

        //just ack the three pings
        getFrame(f);
        sendAck(ACK_PING);
        getFrame(f);
        sendAck(ACK_PING);
        getFrame(f);
        sendAck(ACK_PING);
    }
    else{

        udpSocket.setRecipient(cfg->sendto_url);

        //get the average time of three pings
        timeval time;
        double t1, t2;
        double averageTime = 0.0;

        int ack;

        f->flag = PING;
        f->seq  = ACK_PING;

        //first time...
        t1 = currentTime(&time);
        sendFrame(f);
        ack = getAck();
        t2 = currentTime(&time);
        averageTime += t2 - t1;


        //second time...
        t1 = currentTime(&time);
        sendFrame(f);
        ack = getAck();
        t2 = currentTime(&time);
        averageTime += t2 - t1;


        //third time...
        t1 = currentTime(&time);
        sendFrame(f);
        ack = getAck();
        t2 = currentTime(&time);
        averageTime += t2 - t1;

        averageTime = averageTime / 3;

        //multiply by 20 because Tan says so
        cfg->timeout = averageTime * 20.0;

        cout << "Calculated timeout interval is " << cfg->timeout << " seconds." << endl;
    }

    destroyFrame(f);
    free(f);
}

void SlidingProtocol::getFrame(SlidingProtocol::frame * frame){

    char buffer[buffer_size];
    bool badChecksum = true;
    unsigned short expected_checksum;

    while( badChecksum ){
        //wait for transmission...

        bzero(buffer, buffer_size);
        udpSocket.getData(buffer, buffer_size);

        if( cfg->encryption ){
            decrypt(buffer, buffer_size);
        }

        //get the expected checksum
        expected_checksum = SlidingProtocol::checksum(buffer, checksummed_buf_size);

        bufToStruct(frame, buffer);

        //ignore this transmission if the checksum was bad
        badChecksum = expected_checksum != frame->checksum;

        if( badChecksum ){
            cout << "XXX Checksum failed: " << expected_checksum << " != " << frame->checksum << endl;
        }
    }
}

/*
 * function to return a checksum for a buffer.
 * note that the buffer must be padded to a 16-bit multiple.
 * The parameter bufsize is the number of bytes in buf.
 *
 * This is based on the code in page 95 of our textbook
 */
unsigned short SlidingProtocol::checksum(char * buffer, int bufsize){

    unsigned short * buf = (unsigned short *)buffer;

    int count = bufsize / 2; //number of 16 bit units

    register unsigned long sum = 0;

    while( count ){
        sum += htons(*buf);
        buf = buf + 1;

        if( sum & 0xFFFF0000 ){
            //carry occurred, so wrap around
            sum &= 0xFFFF;
            sum = sum + 1;
        }
        count = count - 1;
    }

    return ~(sum & 0xFFFF);
}

/*
 * function that computes the checksum of the data in the buffer
 * and adds it to the correct place
 */
void SlidingProtocol::addChecksum(char * buffer){
    unsigned short cs = checksum(buffer, this->checksummed_buf_size);
    cs = htons(cs);
    memcpy(buffer + this->checksummed_buf_size, &cs, sizeof(cs));
}
