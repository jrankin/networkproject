#ifndef SLIDINGPROTOCOL_H
#define SLIDINGPROTOCOL_H

#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_blowfish.h"
#include <iostream>
#include <map>
#include <time.h>
#include <vector>
#define ACK_SIZE  sizeof(int)

//different flag types for frames
#define END_OF_FILE 'E'
#define START_OF_FILE 'S'
#define ACK 'A'
#define DEFAULT 'D'

// Timeout 


using namespace std;

class SlidingProtocol {

public:
    //types
	typedef struct {
		char * data;
		unsigned short int seq;
        unsigned short dataSize;
		char flag;
        unsigned short checksum;
		double timer;
        bool isAcked;
	} frame;

    //instance variables
    UDPSocket udpSocket;
    int data_size;
    int buffer_size;
    int checksummed_buf_size;
    config_t * cfg;

    //functions
	SlidingProtocol(UDPSocket* udpSocket, config_t * cfg);
	
    size_t fileSize(char *fileName);
    void writeFile(FILE *fp, char *str, int howMany);
	int readFile(FILE *fp, char *str, int howMany, int &dateSize);
	
    void initFrame(frame * f);
    void destroyFrame(frame * f);
    void encrypt(char * encryptedBuffer, int bufsize);
    void decrypt(char * encryptedBuffer, int bufsize);
    void structToBuf(SlidingProtocol::frame * frame, char * buf);
    void bufToStruct(SlidingProtocol::frame * frame, char * buf);
    void sendAck(int sequenceNumber);
    int  getAck();
    void sendFrame(SlidingProtocol::frame * frame);
    void getFrame(SlidingProtocol::frame * frame);
    unsigned short checksum(char * buffer, int bufsize);
    void addChecksum(char * buffer);
    void calibrateTimeout();

    // Virtual Functions.. Must Implement For Extended Classes.
    virtual void startRec() = 0;
    virtual void startSend() = 0;
    virtual void start() = 0;
};

//note: these have to be at the bottom, where the frame stuct is already defined
#define NEXT16(n) (n + 16 - (n % 16))
#define FRAME ((SlidingProtocol::frame *)0)
#define CHECKSUMMED_BUF_SIZE NEXT16((data_size + sizeof(FRAME->seq) + sizeof(FRAME->dataSize) + sizeof(FRAME->flag)))
#define FRAME_SIZE CHECKSUMMED_BUF_SIZE + sizeof(FRAME->checksum)
#endif
