#include <stdio.h>
#include <signal.h>
#include <time.h>
#include "rankinjn_bogled_StopNWait.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_util.h"
#include <pthread.h>

StopNWait::StopNWait(UDPSocket* socket, config_t * config):SlidingProtocol(socket, config) {

    currentFrame = 0;
    sFrame.seq   = 0;
    rFrame.seq   = 0;
    numFrames    = cfg->maxSeq + 1;
}

void StopNWait::start(){

    if (cfg->isReceiver) {
        startRec();
    }
    else {
        isSend = 1;
        udpSocket.setRecipient(cfg->sendto_url);

        startSend();
    }
}

/*
 * note: this is only being called by the sender now.
 * all the receiver does is update sFrame.seq
 */
void StopNWait::makeFrame(FILE * fp){

    //initialize data
    bzero(sFrame.data, data_size);

	// This Variable gets Changed When passed ...
	int dataSize = 0;
	
	// The result tells when it is done.
    int result = SlidingProtocol::readFile(fp, sFrame.data, data_size, dataSize);

	sFrame.dataSize = dataSize;
    //if this is the end of the file, flag it
    if (result == 1){
        sFrame.flag = END_OF_FILE;
        isSend = 0;
    }
    else {
        sFrame.flag = DEFAULT;
    }

    sFrame.seq = currentFrame;
}

void StopNWait::timeout(){
	int NUM_SECONDS = 3;
	
	double time_counter = 0;

    double timeNow, timeStart;
    timeval time;
    timeStart = currentTime(&time);
    
	clock_t this_time = clock();
    clock_t last_time = this_time;

	while(true) {

        timeNow = currentTime(&time);
		
		if ( rFrame.seq  == currentFrame  ) {
			//cout << endl;
			cout << "received ack for #" << currentFrame << endl << endl;
			keepSending = 0;
			return ;
		}

		if(timeStart + cfg->timeout < timeNow){
			return;
		}
	}
}

void StopNWait::startSend(){
    initFrame(&rFrame);
	rFrame.seq = -1;

	pthread_t thread;
	pthread_create(&thread, NULL, StopNWait::threadGetFrame, this);
			
    FILE *fp;
    fp=fopen(cfg->fileName, "rb");

    //send the first packet to signal start of file upload
    initFrame(&sFrame);
    strcpy(sFrame.data, cfg->fileName);
    int ds = string(cfg->fileName).length();
    sFrame.dataSize = ds > data_size ? data_size : ds;
    sFrame.flag = START_OF_FILE;

    sFrame.seq = 0;

	while( isSend ) {
		keepSending = 1;
		if (sFrame.flag != START_OF_FILE ) { // for file Name, dont make a Frame
			makeFrame(fp);
		}
	
		while(keepSending) {
			sendFrame(&sFrame);
			cout << "Sending Frame:" << currentFrame << endl ;
			timeout();
		}
		if ( sFrame.flag == START_OF_FILE ) {
			sFrame.flag = DEFAULT;
		}
		
		// Update current Frame
		currentFrame = (currentFrame + 1) % numFrames;
	}

    //close the file stream
    fclose(fp);

    //free memory
    destroyFrame(&rFrame);
	
	cout << "File sent.." << endl;
}

void * StopNWait::threadGetFrame(void * stpnWait){
	while(true) {
        ((StopNWait *) stpnWait)->rFrame.seq = ((StopNWait *) stpnWait)->getAck();
	}

    return NULL;
}

void StopNWait::startRec() {

	int isRec = 1;
	cout << "waiting for packets" << endl;

    FILE * fp2;
    string saveFilePath = "";
	currentFrame = 0;

    //init rFrame so it can be written to
    initFrame(&rFrame);

	while(isRec) {

		getFrame(&rFrame);

        cout << "Sending ack:" << rFrame.seq  << endl;
        sendAck(rFrame.seq);

        /*
         * if this is the packet we want, write data and advance
         */
		if (currentFrame == rFrame.seq) {
			
			currentFrame = (currentFrame + 1) % numFrames;

            switch ( rFrame.flag ) {
                case START_OF_FILE:
					saveFilePath.append(rFrame.data, 0, rFrame.dataSize);
					saveFilePath = "/tmp/outDJ";
                    fp2=fopen((char *) "/tmp/outDJ", "wb");
                    cout << "Writing to " << saveFilePath << endl;
					currentFrame = 1;
                    break;

                case END_OF_FILE:
                    cout << "ending file..." << endl;
                    SlidingProtocol::writeFile(fp2, rFrame.data, rFrame.dataSize);
                    currentFrame = 0; //restart cycle
                    fclose(fp2);
                    cout << endl << "File transfer for " << saveFilePath << " is complete." << endl;
					break;

                case DEFAULT:
                    SlidingProtocol::writeFile(fp2, rFrame.data, rFrame.dataSize);
                    break;

                default:
                    cout << "ERROR: bad flag value of " << rFrame.flag << endl;
                    exit(1);
                    break;
            }
		}
	}

    //free memory
    destroyFrame(&rFrame);
}

