#ifndef STOPNWAIT_H
#define STOPNWAIT_H

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_blowfish.h"
#include "rankinjn_bogled_SlidingProtocol.h"

using namespace std;


class StopNWait : public SlidingProtocol 
{

public:
    StopNWait(UDPSocket* udpSocket, config_t * cfg);
	void startRec();
	void startSend();
	void makeFrame(FILE * fp);
    void start();
    static void* threadGetFrame(void *frame);
	
	// Variables
	int keepSending;
	int currentFrame;
	void timeout();
	int isSend;
    int numFrames;
	frame sFrame; //sending frame
	frame rFrame; //receiving frame
};
#endif
