#include "rankinjn_bogled_UDPSocket.h"
#include "rankinjn_bogled_netdefine.h"
#include <stdio.h>
#include <stdlib.h>
#include "rankinjn_bogled_SlidingProtocol.h"

// Constructor
UDPSocket::UDPSocket() {

    //initialize fields
    slen=sizeof(cli_addr);
}


void UDPSocket::getData(char* data, int length) {
    if (recvfrom(sockfd, data, length, 0, (struct sockaddr*)&cli_addr, &slen)==-1){
        err((char *)"recvfrom()");
    }
}

void UDPSocket::sendData(char* data, int length) {
    if (sendto(sockfd, data, length, 0, (struct sockaddr*)&cli_addr, slen)==-1){
        err((char *) "sendto()");
    }
}

void UDPSocket::getPort() {
	bzero(&cli_addr, sizeof(cli_addr));
	cli_addr.sin_family = AF_INET;
	cli_addr.sin_port = htons(PORT);
}

void UDPSocket::getSocket(){
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        err((char *)"socket");
    }
}

string UDPSocket::getIP(){
	return string(inet_ntoa(cli_addr.sin_addr));
}

void UDPSocket::bindToSocket(){
	bzero(&my_addr, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	
  bool success = false;
  int port = PORT;

  while( !success ){
    my_addr.sin_port = htons(port);
    if (bind(sockfd,(struct sockaddr* ) &my_addr, sizeof(my_addr))==-1){
      err((char *)"bind");
      port++;
    }
    else{
      success = true;
    }
  }

	cout << "\nListening on port " << ntohs(my_addr.sin_port) << endl;
}

void UDPSocket::bindToSocketIDA(){
}

void UDPSocket::setRecipient(char* hostname){
  struct hostent *server;
  server = gethostbyname(hostname);
  if (server == NULL) {
    fprintf(stderr,"ERROR, no such host as %s\n", hostname);
    exit(0);
  }
  bcopy(
    (char *)server->h_addr, 
    (char *)&(this->cli_addr.sin_addr.s_addr), 
    server->h_length
  );
}

/*
void UDPSocket::getStruct(SlidingProtocol::frame *frame) {
    if (recvfrom(sockfd, frame, sizeof(SlidingProtocol::frame), 0, (struct sockaddr*)&cli_addr, &slen)==-1){
        err((char *)"recvfrom()");
    }

    frame->seq = ntohl(frame->seq);
    frame->fileSize = ntohl(frame->fileSize);
}

void UDPSocket::sendStruct(SlidingProtocol::frame * frame) {
  
    frame->seq = htonl(frame->seq);
    frame->fileSize = htonl(frame->fileSize);

    if (sendto(sockfd, frame, sizeof(SlidingProtocol::frame), 0, (struct sockaddr*)&cli_addr, slen)==-1){
        err((char *) "sendto()");
    }

    frame->seq = ntohl(frame->seq);
    frame->fileSize = ntohl(frame->fileSize);
}
*/
