#ifndef UDPSocket_H
#define UDPSocket_H

#include "rankinjn_bogled_netdefine.h"
#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_blowfish.h"

using namespace std;

class UDPSocket {

private:

protected:
	
public:
    //functions
    UDPSocket();
    void getSocket();
    void bindToSocket();
    void bindToSocketIDA();
    void getData(char*, int);
    void sendData(char*, int);
    void setRecipient(char* hostname);
    void close();
    void getPort();
	string getIP();

    //instance variables
    int sockfd; 
    socklen_t slen;
    struct sockaddr_in my_addr;
    struct sockaddr_in cli_addr;
    struct hostent *servermake ;
};
#endif
