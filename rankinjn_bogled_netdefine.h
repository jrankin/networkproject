#ifndef ___NETDEFINE_H__
#define ___NETDEFINE_H__

#include <map>
#include <list>
#include <deque>
#include <semaphore.h>
#include <sstream>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> 
#include <boost/functional/hash.hpp>
#include "boost/lexical_cast.hpp"
#include "rankinjn_bogled_blowfish.h"

using namespace std;

#define KDC 2
#define SERVER 1
#define CLIENT 0
#define BUFLEN 512
#define PORT 9933

#define bzero(b,n) memset(b,0,n)
#define bcopy(b1,b2,n) memcpy(b2,b1,n)
#define bcmp(b1,b2,n) memcmp(b1,b2,n)

//these macros are for specifying the size of stuff 
//to send in the key distro algorithm
#define ID_LEN        16
#define NONCE_LEN     8
#define KEY_LEN       16
#define A_ENC_LEN     72
#define B_ENC_LEN     32
#define NONCE_ENC_LEN 16

#define ACK_SIZE sizeof(int)
#define DATA_MAX 65482
#define MAX_WINDOW_SIZE 64

//different flag types for frames
#define END_OF_FILE 'E'
#define START_OF_FILE 'S'
#define ACK 'A'
#define DEFAULT 'D'
#define PING 'P'

#define ACK_PING -1

typedef struct {
    enum {SW, GBN, SR} algorithm;
    int data_size;
    int sendingWindowSize;
    int receivingWindowSize;
    int maxSeq;
    double timeout;
    bool encryption;
    enum {OFF, AUTO, MANUAL} errors;
    double packetLostRatio;
    double checksumRatio;
    deque<int> * packetsLost;
    deque<int> * checksumErrors;
    char * sendto_url;
    char * fileName;
    bool isReceiver;
    Blowfish * BF;
    char * sessionKey;
} config_t;

#endif
