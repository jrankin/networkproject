#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> 
#include <stdio.h>
#include <cstdlib>
#include <deque>
#include <sys/time.h>

#include "rankinjn_bogled_util.h"
#include "rankinjn_bogled_blowfish.h"
#include "boost/lexical_cast.hpp"
#include "rankinjn_bogled_KDC.h"

double currentTime(timeval * t){
    gettimeofday(t, NULL);
    return t->tv_sec + (t->tv_usec/1000000.0);
}

string getNonceFromUser(string message){
    int n;
    std::ostringstream ostr;

    cout << message;
    cin >> n;
    while( cin.fail() || n < -9999999 || n > 99999999 ){
        if( cin.fail() ){
            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');
        }
        cout << "Please enter an 8-digit integer: ";
        cin >> n;
    }

    ostr << n;
    return ostr.str();
}

string getKeyFromUser(string message){
    string s = "";
    cout << message;
    cin >> s;

    while( cin.fail() || s.size() == 0 || s.size() > KEY_LEN ){
        if( cin.fail() ){
            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');
        }
        cout << "Please enter a key within " << KEY_LEN << " characters: ";
        cin >> s;
    }

    return s;
}

void configFromUser(config_t * cfg){

	cout << endl;
	cout << endl;
	cout << "=====================================" << endl;
	cout << "=       Sliding Protocol Setup      =" << endl;
	cout << "=====================================" << endl;
	cout << endl;

    string   s = "";
    int      n = -1;
    double   d = -1;

    deque<int> * packetsLost = new deque<int>();
    deque<int> * checksumErrors = new deque<int>();
    cfg->packetsLost = NULL;
    cfg->checksumErrors = NULL;

	
    cout << "Select Algorithm (1=Stop 'n Wait; 2=Go Back N; 3=Selective Repeat)" << endl;
    cout << "Algorithm: ";
    cin >> n;

    while( n < 1 || n > 3 ){
        if( cin.fail() ){
            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');
        }
        cout << "Please enter 1, 2, or 3: ";
        cin >> n;
    }
    if( n == 1 ){
        cfg->algorithm = config_t::SW;
    }
    else if( n == 2 ){
        cfg->algorithm = config_t::GBN;
    }
    else if( n == 3 ){
        cfg->algorithm = config_t::SR;
    }

    cout << "Enter Payload Size (In bytes; min = 1, max = " << DATA_MAX << ")" << endl;
    cout << "Payload: ";
    cin >> n;

    while( n < 1 || n > DATA_MAX ){
        if( cin.fail() ){
            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');
            n = -1;
        }
        cout << "Enter a number >= 1 and <= " << DATA_MAX << ": ";
        cin >> n;
    }
    cfg->data_size = n;

    //get the window sizes if necessary
    if( cfg->algorithm != config_t::SW ){

        cout << "Enter sending window size (min = 1, max = " << MAX_WINDOW_SIZE << ")" << endl;
        cout << "Size: ";
        cin >> n;

        while( n < 1 || n > MAX_WINDOW_SIZE ){
            if( cin.fail() ){
                cin.clear();
                cin.ignore(std::numeric_limits<int>::max(),'\n');
                n = -1;
            }
            cout << "Enter a number >= 1 and <= " << MAX_WINDOW_SIZE << ": ";
            cin >> n;
        }
        cfg->sendingWindowSize = n;

        if( cfg->algorithm == config_t::SR ){
            cout << "Enter receiving window size (min = 1, max = " << MAX_WINDOW_SIZE << ")" << endl;
            cout << "Size: ";
            cin >> n;

            while( n < 1 || n > MAX_WINDOW_SIZE ){
                if( cin.fail() ){
                    cin.clear();
                    cin.ignore(std::numeric_limits<int>::max(),'\n');
                    n = -1;
                }
                cout << "Enter a number >= 1 and <= " << MAX_WINDOW_SIZE << ": ";
                cin >> n;
            }
            cfg->receivingWindowSize = n;
        }
        else{
            cfg->receivingWindowSize = 1;
        }
    }

    cout << "Enter maximum sequence number " << endl;
    cout << "Max: ";
    cin >> n;

    int lowerBound = cfg->sendingWindowSize + cfg->receivingWindowSize;
    if( cfg->algorithm == config_t::SW ){
        lowerBound = 1;
    }

    while( n < lowerBound ){
        if( cin.fail() ){
            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');
            n = -1;
        }
        cout << "Enter a number >= " << lowerBound << ": ";
        cin >> n;
    }
    cfg->maxSeq = n;


    /*
    if( !cfg->isReceiver ){
        cout << "Enter the timeout interval (in seconds; minimum of 1 second)" << endl;
        cout << "Timeout: ";
        cin >> d;

        while( n < 1 ){
            if( cin.fail() ){
                cin.clear();
                cin.ignore(std::numeric_limits<int>::max(),'\n');
                n = -1;
            }
            cout << "Enter an integer > 0: ";
            cin >> n;
        }
        cfg->timeout = n;
    }
    */

    while( s.size() == 0 ){
        cin.clear();
        cout << "Use encryption? (y/n): ";
        cin >> s;
        if( s.find("y") != string::npos || s.find("Y") != string::npos ){
            cfg->encryption = true;
        }
        else if( s.size() > 0 ){
            cfg->encryption = false;
        }
    }

    cout << "Select error simulation (0=off, 1=auto, 2=manual)" << endl;
    cout << "Errors: ";
    cin >> n;

    while( n < 0 || n > 2 ){
        if( cin.fail() ){
            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');
            n = -1;
        }
        cout << "Choose 0, 1, or 2: ";
        cin >> n;
    }
    if( n == 0 ){
        cfg->errors = config_t::OFF;
    }
    else if( n == 1 ){
        cfg->errors = config_t::AUTO;
    }
    else if( n== 2 ){
        cfg->errors = config_t::MANUAL;
    }

    if( cfg->errors == config_t::AUTO ){

        while( d < 0 || d > 1 ){
            cout << "Enter " << (cfg->isReceiver ? "ack" : "packet") << " lost rate (decimal between 0, 1): ";
            cin >> d;
            if( cin.fail() ){
                cin.clear();
                cin.ignore(std::numeric_limits<int>::max(),'\n');
                d = -1;
            }
        }
        cfg->packetLostRatio = d;

        if( !cfg->isReceiver ){
            d = -1;
            while( d < 0 || d > 1 ){
                cout << "Enter packet corrupted rate (decimal between 0, 1): ";
                cin >> d;
                if( cin.fail() ){
                    cin.clear();
                    cin.ignore(std::numeric_limits<int>::max(),'\n');
                    d = -1;
                }
            }
            cfg->checksumRatio = d;
        }
    }
    else if( cfg->errors == config_t::MANUAL ){

        if( cfg->isReceiver ){
            cout << "Enter the sequence #'s of the acks to be lost. Enter non-number to terminate." << endl;
            while (cin >> n){
                packetsLost->push_back(n);
            }
        }
        else{

            cout << "Enter the sequence #'s of the packets to be dropped. Enter non-number to terminate." << endl;
            while (cin >> n){
                packetsLost->push_back(n);
            }

            cin.clear();
            cin.ignore(std::numeric_limits<int>::max(),'\n');

            cout << "Enter the sequence #'s of the checksum errors. Enter non-number to terminate." << endl;
            while (cin >> n){
                checksumErrors->push_back(n);
            }
        }

        cfg->packetsLost = packetsLost;
        cfg->checksumErrors = checksumErrors;
    }
	

	if( !cfg->isReceiver ){
        s = "";
        cin.clear();
        cin.ignore(std::numeric_limits<int>::max(),'\n');
		cout << "Please Enter a File Path: ";
		cin >> s;

        //if file doesn't exist
        if( access(s.c_str(), F_OK) == -1 ){
            cout << "Could not find file " << s << endl;
            s = "";
        }
        while( s.size() == 0 ){
            cout << "Please enter a file path: ";
            cin >> s;
            if( access( s.c_str(), F_OK ) == -1 ){
                cout << "Could not find file " << s << endl;
                s = "";
            }
        }
        if( s.size() > (unsigned)cfg->data_size ){
            cout << "Warning: filename will be truncated to fit in payload." << endl;
        }
        cfg->fileName = (char*)malloc(s.size());
        strcpy(cfg->fileName, s.c_str());
	}
}

string getRandom(int number) {

  //seed the generator with the number
  srand(number);

  cout << "number: " << number << endl;

  //get a random int with NONCE_LEN digits in it
  int g = rand() % (int)pow(10.0, NONCE_LEN-1.0);
  g = g * g * g;
	string x  = boost::lexical_cast<string>(g);

	return x;
}

string parseInt(int x) {
	return  boost::lexical_cast<string>(x);
}

string nonceHash(string nonce){
    string result = "";

    try{
        int num = boost::lexical_cast<int>(nonce);
        num = (num << 22) + (num << 10) + num;
        num = num % 10000000; //make sure it's capped at 8 digits
        result = boost::lexical_cast<string>(num);
    }
    catch( boost::bad_lexical_cast & e ){
        result = "NaN";
    }

    return result;
}

void err(char *s){
  perror(s);
  exit(1);
}

void getSocket(int *sockfd){
	if ((*sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
    err((char *)"socket");
  }
}

void bindToSocket(sockaddr_in *my_addr, int *sockfd){
	bzero(my_addr, sizeof(*my_addr));
	my_addr->sin_family = AF_INET;
	my_addr->sin_port = htons(PORT);
	my_addr->sin_addr.s_addr = htonl(INADDR_ANY);
	
	if (bind(*sockfd, (struct sockaddr* ) my_addr, sizeof(*my_addr))==-1){
    err((char *)"bind");
  }
}

/*
 * handy little function to return the next multiple of 8
 * given an arbitrary number. Needed this because our blowfish
 * algorithm encrypts/decripts sizes that are multiples of
 * 8 bytes
 */
int nextMult8(int size){
    return size + ( 8 - ( size % 8 ) );
}

/*
 * string_to_hex is used because we need a way to safely print
 * encrypted data to the terminal without triggering control characters.
 */
std::string string_to_hex(char *input, size_t len){
  static const char* const lut = "0123456789ABCDEF";

  std::string output;
  output.reserve(2 * len);

  for (size_t i = 0; i < len; ++i){
    const unsigned char c = input[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 15]);
  }

  return output;
}

/*
 * prompts users with message,
 * gets string from user and returns it
 */
std::string getInputString(string message){
  string input;
  cout << message;
  cin >> input;

  return input;
}

/*
 * custom encryption function to handle the endian-ness problem
 */
void encrypt(Blowfish *BF, int* buffer, unsigned int nbytes){

  for( unsigned int i=0; i<nbytes/sizeof(int); i++ ){
    buffer[i] = htonl(buffer[i]);
  }

  BF->Encrypt(buffer, nbytes);

  for( unsigned int i=0; i<nbytes/sizeof(int); i++ ){
    buffer[i] = ntohl(buffer[i]);
  }
}

void decrypt(Blowfish *BF, int* buffer, unsigned int nbytes){

  for( unsigned int i=0; i<nbytes/sizeof(int); i++ ){
    buffer[i] = htonl(buffer[i]);
  }

  BF->Decrypt(buffer, nbytes);

  for( unsigned int i=0; i<nbytes/sizeof(int); i++ ){
    buffer[i] = ntohl(buffer[i]);
  }
}

bool isLittleEndian(){
  return 1 != htonl(1);
}

/*
 * custom function to swap endian-ness of a byte, used by our
 * encryption function
 */
void swapBytes(char* buffer){
  char one = buffer[0];
  char two = buffer[1];
  buffer[0] = buffer[3];
  buffer[1] = buffer[2];
  buffer[2] = two;
  buffer[3] = one;
}
