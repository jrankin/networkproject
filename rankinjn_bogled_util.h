#ifndef ___UTIL_H___
#define ___UTIL_H___

#include "rankinjn_bogled_netdefine.h"
#include "rankinjn_bogled_blowfish.h"

using namespace std;

void doAlgorithm(int sockfd, int client_or_server, struct sockaddr_in serv_addr, socklen_t slen);
string step1(string requestName);
string step2();
void err(char *s);
void getSocket(int *sockfd);
void bindToSocket(sockaddr_in *myaddr, int *sockfd);
int nextMult8(int size);
string string_to_hex(char *input, size_t len);
int hashCode(string x);
string getRandom(int number);
string nonceHash(string nonce);
void encrypt(Blowfish *BF, int* buffer, unsigned int nbytes);
void decrypt(Blowfish *BF, int* buffer, unsigned int nbytes);
bool isLittleEndian();
void swapBytes(char* buffer);
string parseInt(int x);
void configFromUser(config_t * cfg);
string getNonceFromUser(string message);
string getKeyFromUser(string message);
double currentTime(timeval * t);

#endif
